<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }

    public function welcome(Request $request){
        // var_dump($request);
        // dd($request["nama"]);
        return view('menuUtama');
    }

    public function welcome_post(Request $request){
        // dd($request->all());
        $namaDepan = $request["namad"];
        $namaBelakang = $request["namab"];
        return view('menuUtama', compact('namaDepan','namaBelakang'));
    }
}
 