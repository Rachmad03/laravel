<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/menuUtama" method="POST">
        @csrf
        <label for="">First Name:</label><br><br>
        <input type="text" name="namad"><br><br>

        <label for="">Last Name:</label><br><br>
        <input type="text" name="namab"><br><br>

        <label for="">Gender:</label><br><br>
        <input type="radio" name="jk">Male <br>
        <input type="radio" name="jk">Female <br>
        <input type="radio" name="jk">Other <br><br>

        <label for="">Nationality:</label><br><br>
        <select name="bangsa" id="">
            <option value="indo">Indonesia</option>
            <option value="sin">Singapura</option>
            <option value="ger">Germany</option>
            <option value="aus">Australia</option>
        </select> <br><br>

        <label for="">Language Spoken:</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br><br>
        <input type="checkbox"> English <br><br>
        <input type="checkbox"> Other <br><br>

        <label for="">Bio:</label>  <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit">

    </form>
</body>
</html>